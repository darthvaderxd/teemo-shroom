const restify = require('restify-clients');
const region = require('./region');

/* istanbul ignore next line */
let apiKey = process.env.RIOT_API_KEY || '';

const getEndPointForRegion = (request) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (request) {
        const endPoint = region.getEndpoint(request);
        if (endPoint.result === false) {
            result.message = endPoint.message;
        } else {
            result.result = endPoint.result;
            result.message = '';
        }
    } else {
        result.message = 'a region is required';
    }

    return result;
};

const getApiKey = () => apiKey;

const generateLinkForRegion = (request) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    const endPoint = getEndPointForRegion(request);

    if (endPoint.result === false) {
        result.message = endPoint.message;
    } else {
        result.result = {
            link: `https://${endPoint.result.endpoint}`,
            region: request,
            name: endPoint.result.name,
        };
        result.message = '';
    }

    return result;
};

const performRequest = (client, options) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    return new Promise((resolve) => {
        client.get(options, (err, req, res, obj) => {
            /* istanbul ignore next */
            if (err) {
                console.error(err);
                result.message = err;
                if (typeof err.message !== 'undefined') {
                    result.message = err.message;
                    if (typeof err.message.status !== 'undefined') {
                        result.message = err.message.status;
                    }
                }
                return resolve(result);
            }

            result.result = obj;
            result.message = '';
            return resolve(result);
        });
    });
};

const sendRequest = async (endPointData, endPoint) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (endPointData && endPoint) {
        if (typeof endPoint !== 'object') {
            result.message = 'expected endpoint to be an object';
        } else {
            const client = restify.createJsonClient(endPointData.link);
            const options = {
                path: endPoint.result,
                headers: {
                    'X-Riot-Token': getApiKey(),
                },
                retry: {
                    retries: 0,
                },
                agent: false,
            };

            const data = await performRequest(client, options);

            /* istanbul ignore next */
            if (data.result) {
                result.result = data.result;
                result.message = '';
            } else { /* istanbul ignore next */
                result.message = data.message;
            }
        }
    }

    return result;
};

// these are mostly here for testing and if you wanted to do osomething manually
module.exports.getEndPointForRegion = getEndPointForRegion;
module.exports.getApiKey = getApiKey;
module.exports.generateLinkForRegion = generateLinkForRegion;

module.exports.setApiKey = (key) => {
    apiKey = key;
};

module.exports.request = async (request, endPoint) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    // TODO: This is kind of gross...Clean up?
    if (request && endPoint) {
        const endPointData = generateLinkForRegion(request);

        if (endPointData.result) {
            const data = await sendRequest(endPointData.result, endPoint);
            // todo
            if (data.result) {
                result.result = data.result;
                result.message = '';
            } else {
                result.message = data.message;
            }
        } else {
            result.message = endPointData.message;
        }
    } else {
        result.message = 'region and endpoint required';
    }

    return result;
};
