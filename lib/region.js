const regions = {
    br: {
        endpoint: 'br1.api.riotgames.com',
        name: 'Brazil',
    },
    eune: {
        endpoint: 'eun1.api.riotgames.com',
        name: 'EU North',
    },
    euw: {
        endpoint: 'euw1.api.riotgames.com',
        name: 'EU West',
    },
    jp: {
        endpoint: 'jp1.api.riotgames.com',
        name: 'Japan',
    },
    kr: {
        endpoint: 'kr.api.riotgames.com',
        name: 'Korea',
    },
    lan: {
        endpoint: 'la1.api.riotgames.com',
        name: 'Latin America North',
    },
    las: {
        endpoint: 'la2.api.riotgames.com',
        name: 'Latin America South',
    },
    na: {
        endpoint: 'na1.api.riotgames.com',
        name: 'North America',
    },
    oce: {
        endpoint: 'oc1.api.riotgames.com',
        name: 'Oceanic',
    },
    tr: {
        endpoint: 'tr1.api.riotgames.com',
        name: 'Turkey',
    },
    ru: {
        endpoint: 'ru.api.riotgames.com',
        name: 'Russia',
    },
    pbe: {
        endpoint: 'pbe1.api.riotgames.com',
        name: 'PBE',
    },
};

module.exports.getRegions = () => regions;

module.exports.getEndpoint = (region) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (region) {
        result.result = regions[region] || false;
        if (result.result === false) {
            result.message = 'specified region not found';
        } else {
            result.message = '';
        }
    } else {
        result.message = 'a region is required';
    }

    return result;
};
