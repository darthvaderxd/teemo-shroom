const champion = require('./endpoints/champion');
const league = require('./endpoints/league');
const mastery = require('./endpoints/mastery');
const match = require('./endpoints/match');
const spectator = require('./endpoints/spectator');
const status = require('./endpoints/status');
const summoner = require('./endpoints/summoner');

const modules = {
    champion: {
        active: true,
        name: 'Champions',
        module: champion,
    },
    league: {
        active: true,
        name: 'League',
        module: league,
    },
    match: {
        active: true,
        name: 'Matches',
        module: match,
    },
    mastery: {
        active: true,
        name: 'Champion Mastery',
        module: mastery,
    },
    spectator: {
        active: true,
        name: 'List of Spectable Games',
        module: spectator,
    },
    status: {
        active: false,
        name: 'Status',
        module: status,
    },
    summoner: {
        active: true,
        name: 'Summoner',
        module: summoner,
    },
    inactive: {
        active: false,
        name: 'Inactive Test',
        module: null,
    },
};

module.exports.getModules = () => modules;

module.exports.getModule = (name) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!name) {
        result.message = 'a module name is required';
    } else {
        result.result = modules[name] || false;

        if (result.result && result.result.active === false) {
            result.result = false;
            result.message = 'module is inactive';
        } else if (result.result === false) {
            result.message = 'module does not exist';
        } else {
            result.message = '';
        }
    }

    return result;
};
