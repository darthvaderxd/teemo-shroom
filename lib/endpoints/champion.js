const route = '/lol/platform/v3/champions/:id';

module.exports = (id) => {
    const result = {
        result: route,
        message: '',
    };

    const replaceWhat = !id ? '/:id' : ':id';
    const replaceWith = !id ? '' : id;
    result.result = result.result.replace(replaceWhat, replaceWith);

    return result;
};
