const routes = {
    byQueue: '/lol/league/v3/:rank/by-queue/:queueType',
    byLeague: '/lol/league/v3/leagues/:leagueId',
    bySummonerId: '/lol/league/v3/positions/by-summoner/:summonerId',
};

const ranks = {
    challenger: {
        name: 'Challenger Teir',
        value: 'challengerleagues',
    },
    master: {
        name: 'Master Teir',
        value: 'masterleagues',
    },
};

const queues = {
    solo: {
        name: 'Ranked Solo/Duo',
        value: 'RANKED_SOLO_5x5',
    },
    flex: {
        name: 'Ranked Flex',
        value: 'RANKED_FLEX_SR',
    },
    twisted: {
        name: 'Ranked Twisted Treeline',
        value: 'RANKED_FLEX_TT',
    },
};

module.exports.getQueues = () => ({ result: queues, message: '' });
module.exports.getRanks = () => ({ result: ranks, message: '' });

module.exports.byQueue = (rank, queueType) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!rank) {
        result.message = 'you must specifiy a rank';
    } else if (!queueType) {
        result.message = 'you must specifiy a queueType';
    } else if (typeof ranks[rank] === 'undefined') {
        result.message = 'the rank you listed doesn\'t exist';
    } else if (typeof queues[queueType] === 'undefined') {
        result.message = 'the queueType you listed doesn\'t exist';
    } else {
        const link = routes.byQueue.replace(':rank', ranks[rank].value);
        result.result = link.replace(':queueType', queues[queueType].value);
        result.message = '';
    }

    return result;
};

module.exports.byLeague = (leagueId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!leagueId) {
        result.message = 'you must specify a leagueId';
    } else if (typeof leagueId !== 'string') {
        result.message = 'you must specify leagueId as a string';
    } else {
        result.result = routes.byLeague.replace(':leagueId', leagueId);
        result.message = '';
    }

    return result;
};

module.exports.bySummonerId = (summonerId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerId) {
        result.message = 'you must specifiy a summonerId';
    } else {
        result.result = routes.bySummonerId.replace(':summonerId', summonerId);
        result.message = '';
    }

    return result;
};
