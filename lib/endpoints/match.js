const routes = {
    byAccountId: '/lol/match/v3/matchlists/by-account/:accountId',
    byMatchId: '/lol/match/v3/matches/:matchId',
    byMatchWithTimeline: '/lol/match/v3/timelines/by-match/:matchId',
    byTournamentId: '/lol/match/v3/matches/by-tournament-code/:tournamentId/ids',
    byMatchIdAndTournamentId: '/lol/match/v3/matches/:matchId/by-tournament-code/:tournamentId',
};

module.exports.byAccountId = (accountId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!accountId) {
        result.message = 'you must specify an accountId';
    } else {
        result.result = routes.byAccountId.replace(':accountId', accountId);
        result.message = '';
    }

    return result;
};

module.exports.byMatchId = (matchId, withTimelines) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!matchId) {
        result.message = 'you must specify a matchId';
    } else if (!withTimelines) {
        result.result = routes.byMatchId.replace(':matchId', matchId);
        result.message = '';
    } else {
        result.result = routes.byMatchWithTimeline.replace(':matchId', matchId);
        result.message = '';
    }

    return result;
};

module.exports.byTournamentId = (tournamentId, matchId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!tournamentId) {
        result.message = 'you must specifiy a tournamentId';
    } else if (tournamentId && !matchId) {
        result.result = routes.byTournamentId.replace(':tournamentId', tournamentId);
        result.message = '';
    } else {
        const link = routes.byMatchIdAndTournamentId.replace(':tournamentId', tournamentId);
        result.result = link.replace(':matchId', matchId);
        result.message = '';
    }

    return result;
};
