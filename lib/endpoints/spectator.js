const routes = {
    bySummonerId: '/lol/spectator/v3/active-games/by-summoner/:summonerId',
    featuredGames: '/lol/spectator/v3/featured-games',
};

module.exports.bySummonerId = (summonerId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerId) {
        result.message = 'you must specify an summonerId';
    } else {
        result.result = routes.bySummonerId.replace(':summonerId', summonerId);
        result.message = '';
    }

    return result;
};

module.exports.featuredGames = () => {
    const result = {
        result: routes.featuredGames,
        message: '',
    };

    return result;
};
