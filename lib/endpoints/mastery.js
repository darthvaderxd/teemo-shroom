const routes = {
    bySummonerId: '/lol/champion-mastery/v3/champion-masteries/by-summoner/:summonerId',
    byChampionId: '/lol/champion-mastery/v3/champion-masteries/by-summoner/:summonerId/by-champion/:championId',
    getTotalScoreBySummonerId: '/lol/champion-mastery/v3/scores/by-summoner/:summonerId',
};

module.exports.bySummonerId = (summonerId, championId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerId) {
        result.message = 'you must specify an summonerId';
    } else if (!championId) {
        result.result = routes.bySummonerId.replace(':summonerId', summonerId);
        result.message = '';
    } else {
        const link = routes.byChampionId.replace(':summonerId', summonerId);
        result.result = link.replace(':championId', championId);
        result.message = '';
    }

    return result;
};

module.exports.getTotalScoreBySummonerId = (summonerId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerId) {
        result.message = 'you must specify an summonerId';
    } else {
        result.result = routes.getTotalScoreBySummonerId.replace(':summonerId', summonerId);
        result.message = '';
    }

    return result;
};
