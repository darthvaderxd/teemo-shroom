const routes = {
    byAccountId: '/lol/summoner/v3/summoners/by-account/:accountId',
    bySummonerName: '/lol/summoner/v3/summoners/by-name/:summonerName',
    bySummonerId: '/lol/summoner/v3/summoners/:summonerId',
};

module.exports.byAccountId = (accountId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!accountId) {
        result.message = 'you must specify an accountId';
    } else {
        result.result = routes.byAccountId.replace(':accountId', accountId);
        result.message = '';
    }

    return result;
};

module.exports.bySummonerName = (summonerName) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerName) {
        result.message = 'you must specify an summonerName';
    } else {
        result.result = routes.bySummonerName.replace(':summonerName', summonerName);
        result.message = '';
    }

    return result;
};

module.exports.bySummonerId = (summonerId) => {
    const result = {
        result: false,
        message: 'unknown error',
    };

    if (!summonerId) {
        result.message = 'you must specify an summonerId';
    } else {
        result.result = routes.bySummonerId.replace(':summonerId', summonerId);
        result.message = '';
    }

    return result;
};
