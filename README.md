Teemo Shroom
=======

## Teemo Shroom - An up to date NodeJS Framework for Riot's API (minus tournament endpoints)

## Including in a project

`
const teemo = require('teemo-shroom');
`

## How to set your Riot API Key

You can set an environment variable of `export RIOT_API_KEY=yourkey` and the framework will use this as your key,
so you don't have to hard code it if you don't want to. Just pass that on to the people who use your code. You can also set it with the following:

`
teemo.setApiKey('yourkey');
`

## Functions to note for use in your development
| Function | Description | Arguments | return |
|---|---|---|---|
| setApiKey | Allows you to set your Riot API KEY | key - your api key | N/A |
| getApiKey | For debugging purposes it returns the Riot API Key that is set | N/A | string your Riot API Key that is set |
| getModules | Returns an object of modules lets you know whats enabled and disabled added for developers to be able to debug | N/A | object with objects that contain the following information: boolean active - is module active, string name - The name of the module, object module - functions to use when getting the needed endPointData. IE: {inactive: `{ active: false, name: 'Inactive Test', module: null }` |
| getRegions | Returns an object of regions and data associated with the regions | N/A | object with objects that contain the following information: string endpoint - the hostname for the endpoint, string - name a user friendly name for the object. IE: `{ endpoint: 'na1.api.riotgames.com', name: 'North America' }` |
| getRegion | Returns a specific region listed in getRegions based on the keyname | string region - the region you wish to get endpoint information for. IE: na | an object containing connection information for a given region. IE: `{ endpoint: 'na1.api.riotgames.com', name: 'North America' }` |
| sendRequest | Sends the request to Riot's API | string regionName - the region you wish to query against. IE: na, object endPoint - the result given from calling ritopls.modules.{name}.{function}. See below | object - result of your query if it failed it would be something like this `{ result: false, message: 'reason for failure' }`. If the result succeeded then the result object would not be false and would be the result given by Riot's API |
| modules.champion.module | Returns a list of champions or a single champion based on if you passed in a championId or not | integer championId - the id of the champion you wish to lookup | return will be an object that you would pass in as endPoint for sendRequest (the second param) |
| modules.league.module.getQueues | Returns the queue types you can query on | N/A | return will be an object of objects, and are useful when calling `modules.league.byQueue`, as it expects you to pass the key to one of them in. Example Item: `{ solo: { name: 'Ranked Solo/Duo', value: 'RANKED_SOLO_5x5' } }` |
| modules.league.module.getRanks | Returns the rank types you can query on, useful also for displaying the name to the user wishing to make the query | N/A | return will be an object of objects, and are useful when calling `modules.league.byQueue`, as it expects you to pass the key to one of them in. Example Item: `{ challenger: {  name: 'Challenger Teir', value: 'challengerleagues' }` |
| modules.league.module.byQueue | look up the league for a given queue | string rank - the rank you wish to look up [master, challenger], string queue_type the queue type you want to look up [solo, flex, twisted] | object - the result of your lookup byQueue for challenger/master and the type of queue IE `solo`, needed for sendRequest |
| modules.league.module.byLeague | Look up a league | leagueId - the league you wish to lookup | object - the endPoint information needed for sendRequest |
| modules.league.module.bySummonerId | Look up a summoner | summonerId - the id of the summoner you wish to lookup | object - the endPoint information needed for sendRequest |
| modules.mastery.module.bySummonerId | lookup a summoners champion masteries or a single champion mastery | string summonerId - the summonerId for the summoner you want to look up champion mastery on, string championId - the championId for the champion mastery you want too look at for the summoner | object - the endPoint information needed for sendRequest |
| modules.mastery.module.getTotalScoreBySummonerId | look up the total mastery score for a given summoner | string summonerId - the summonerId for the summoner you want to look up points total for | object - the endPoint information needed for sendRequest |
| modules.match.module.byAccountId | look up matches for a given accountId | string matchid - the accountId you wish to look up matches for | object  - the endPoint information needed for sendRequest |
| modules.match.module.byMatchId | look up a specific match | string matchId - the matchId for the match you wish to look up, boolean withTimeline - do you wish to also have a timeline of the match | object - the endPoint information needed for sendRequest |
| modules.match.module.byTournamentId | look up matches or single match for a given tournament code | string tournamentId - the tournament you wish to look up matches for, string matchId - the match you wish to look at | object - the endPoint information needed for sendRequest |
| modules.spectator.module.bySummonerId | Look up current game information for a given summonerId | string summonerId - the summonerId for the summoner you wish to look up current game information for | object - the endPoint information needed for sendRequest |
| modules.spectator.module.featuredGames | Look up featured games currently going on | N/A | object - the endPoint information needed for sendRequest |
| modules.status.module | get the league of legends status for the given region | N/A | object - endPoint information needed for sendRequest |
| modules.summoner.module.byAccountId | look up summoner information by accountId | string accountId - the accountId for the summoner you wish to look up | object - endPoint information needed for sendRequest |
| modules.summoner.module.bySummonerName | look up summoner information by their name | string summonerName - the summoner name for the summoner you wish to look up | object - endPoint information needed for sendRequest |
| modules.summoner.module.bySummonerId | look up summoner information by there summonerId | string summonerId - the summonerId for the summoner you wish to look up | object -endPoint information needed for sendRequest |

## Ok thats all well and dandy, but whats the code look like to query something

`
const teemo = require('teemo-shroom');

/**
 * Look up a summoner by summoner name
 * @param string region - the region the summoner is apart of ie 'na'
 * @param string name - the summoner name ie 'darthvaderxd'
 * @return object - contains the result of the lookup
 */
const lookupSummonerByName = async (region, name) => {
    const endPoint = ritopls.modules.summoner.module.bySummonerName(name);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = await ritopls.sendRequest(region, endPoint);
    return result;
};
`

For more examples see the examples folder

Checkout this YouTube video for getting started if you find yourself stuck
https://youtu.be/I9C8umvrD8k