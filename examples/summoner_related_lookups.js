const teemo = require('../index');

/**
 * Look up a summoner by summoner name
 * @param string region - the region the summoner is apart of ie 'na'
 * @param string name - the summoner name ie 'darthvaderxd'
 * @return object - contains the result of the lookup
 */
const lookupSummonerByName = async (region, name) => {
    const endPoint = teemo.modules.summoner.module.bySummonerName(name);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = await teemo.sendRequest(region, endPoint);
    return result;
};

/**
 * look up a summoner based on summonerId
 * @param string region - the region the summoner is apart of ie 'na'
 * @param integer summonerId - the number id of the summoner you wish to look up
 * @return object - contains the result of the lookup
 */
const lookupSummonerBySummonerId = async (region, summonerId) => {
    const endPoint = teemo.modules.summoner.module.bySummonerId(summonerId);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = await teemo.sendRequest(region, endPoint);
    return result;
};

/**
 * look up a summoner based on accountId
 * @param string region - the region the summoner is apart of ie 'na'
 * @param integer accountId - the number accountId of the summoner you wish to look up
 * @return object - contains the result of the lookup
 */
const lookupSummonerByAccountId = async (region, accountId) => {
    const endPoint = teemo.modules.summoner.module.byAccountId(accountId);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = await teemo.sendRequest(region, endPoint);
    return result;
};

const runCLIExample = async () => {
    console.log('looking up summoner data for DarthVaderXD');
    const summonerData = await lookupSummonerByName('na', 'DarthVaderXD');
    console.log('Data returned', summonerData);

    console.log('...');
    console.log('looking up summoner data by DarthVaderXD summonerId');
    const summonerIdData = await lookupSummonerBySummonerId('na', summonerData.result.id);
    console.log('Data returned', summonerIdData);

    console.log('...');
    console.log('looking up summoner data by DarthVaderXD accountId');
    const accountData = await lookupSummonerByAccountId('na', summonerData.result.accountId);
    console.log('Data returned', accountData);
};

runCLIExample();
