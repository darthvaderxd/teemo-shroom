const teemo = require('teemo-shroom');

const getSummonerByName = async (region, name) => {
    const endPoint = teemo.modules.summoner.module.bySummonerName(name);
    const result = await teemo.sendRequest(region, endPoint);

    if (result.result === false) {
        console.error(result.message);
        return false;
    }

    return result;
};

const getMatchesForSummoner = async (region, name) => {
    const summonerResult = await getSummonerByName(region, name);

    if (summonerResult === false) {
        return false;
    }

    const endPoint = teemo.modules.match.module.byAccountId(summonerResult.result.accountId);
    const result = await teemo.sendRequest(region, endPoint);

    if (result.result === false) {
        console.error(result.message);
        return false;
    }

    return result;
};

const getMatch = async (region, matchId, withTimeline) => {
    const endPoint = teemo.modules.match.module.byMatchId(matchId, withTimeline);
    const result = await teemo.sendRequest(region, endPoint);

    if (result.result === false) {
        console.error(result.message);
        return false;
    }

    return result;
};

const run = async () => {
    /*
    const summoner = await getSummonerByName('na', 'darthvaderxd');
    console.log('Summoner => ', summoner);
    */

    const matches = await getMatchesForSummoner('na', 'darthvaderxd');
    console.log('matches => ', matches.result);

    const match = await getMatch('na', matches.result.matches[0].gameId/* , true */);
    console.log('specific match => ', match.result);

};


run();