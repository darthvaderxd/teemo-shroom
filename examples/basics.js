const teemo = require('teemo-shroom');

// if you want to set your riot api key
// you can set by environment
// export RIOT_API_KEY=yourriotkey
// or you can set it with a funciton
// teemo.setApiKey('yourkeyhere');

// these are the regions you can work with
const regions = teemo.getRegions();
console.log("regions that we can acceses");
console.log("Regions to pass in  => ", Object.keys(regions));

// the modules
const modules = teemo.getModules();
console.log("Modules we can work with => ", Object.keys(modules));

// working with the modules
// real basic example for champions
/*
const endPoint = teemo.modules.champion.module();

teemo.sendRequest('na', endPoint)
    .then((result) => {
        if (result.result === false) {
            console.error(result.message);
            return false;
        }

        console.log("Champions => \n", result.result.champions);
    }).catch((error) => {
        console.error(error);
    });
*/

// get a single champion
/*
const endPoint = teemo.modules.champion.module(35);
teemo.sendRequest('na', endPoint)
    .then((result) => {
        if (result.result === false) {
            console.error(result.message);
            return false;
        }

        console.log("Champion => \n", result.result);
    }).catch((error) => {
        console.error(error);
    });
*/

// because these are promises you can use async await
/*
const getChampions = async (region, id) => {
    const endPoint = teemo.modules.champion.module(id);
    const result = await teemo.sendRequest(region, endPoint);

    console.log(result);
    return result;
}

getChampions('na');
getChampions('na', 35);
*/