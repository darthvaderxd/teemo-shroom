const teemo = require('../index');

/**
 * Look up a summoner by summoner name
 * @param string region - the region the summoner is apart of ie 'na'
 * @param string name - the summoner name ie 'darthvaderxd'
 * @return object - contains the result of the lookup
 */
const lookupSummonerByName = async (region, name) => {
    const endPoint = teemo.modules.summoner.module.bySummonerName(name);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = await teemo.sendRequest(region, endPoint);
    return result;
};

/**
 * Look up match history for a summoner
 * @param string region - the region the summoner is apart of ie 'na'
 * @param string name - the summoner name ie 'darthvaderxd'
 * @return object - contains the result of the lookup
 */
const getMatchHistoryForSummoner = async (region, name) => {
    const summonerResult = await lookupSummonerByName(region, name);

    if (!summonerResult.result) {
        return summonerResult;
    }

    const matchEndPoint = await teemo.modules.match.module.byAccountId(summonerResult.result.accountId);
    if (matchEndPoint.result === false) {
        return matchEndPoint;
    }

    const result = await teemo.sendRequest(region, matchEndPoint);
    return result;
};

/**
 * look up a particular match by matchId
 * @param string region - the region the summoner is apart of ie 'na'
 * @param integer matchId  - the matchId for the match you want to look up
 * @return object - the result of the query
 */
const lookupMatch = async (region, matchId) => {
    const endPoint = teemo.modules.match.module.byMatchId(matchId);

    if (endPoint.result === false) {
        return endPoint;
    }

    const result = teemo.sendRequest(region, endPoint);
    return result;
};

const runCLIExample = async () => {
    console.log('looking up match history for DarthVaderXD');
    const matches = await getMatchHistoryForSummoner('na', 'DarthVaderXD');
    console.log(matches.result);

    console.log('...');
    console.log('looking up match from match history');
    const match = await lookupMatch('na', matches.result.matches[0].gameId);
    console.log(match);
};

runCLIExample();
