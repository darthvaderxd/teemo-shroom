const client = require('./lib/client');
const modules = require('./lib/modules');
const regionEndpoints = require('./lib/region');

// client functionality
/**
 * The following functions are here for convience however never really
 * meant for you to use
 */
module.exports.getEndPointForRegion = client.getEndPointForRegion;
module.exports.getApiKey = client.getApiKey;
module.exports.generateLinkForRegion = client.generateLinkForRegion;

/**
 * these functions are intended for you to use regularly
 */
module.exports.setApiKey = client.setApiKey;
module.exports.sendRequest = client.request;

// module management
module.exports.getModules = modules.getModules;

// regional endpoint information
module.exports.getRegions = regionEndpoints.getRegions;
module.exports.getRegion = regionEndpoints.getEndpoint;

// endpoint functionality
module.exports.modules = modules.getModules();
