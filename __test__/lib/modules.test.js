const object = require('../../lib/modules');

describe('Module Handler > ./lib/module.js << Test', () => {
    describe('getModules', () => {
        test('getModules returns an object and formatted as expected', () => {
            const result = object.getModules();
            const resultKeys = Object.keys(result);
            const expectedKeys = ['active', 'name', 'module'];
            expect(typeof result).toBe('object');

            resultKeys.forEach((key) => {
                const mod = result[key];
                expect(typeof mod).toBe('object');

                const keys = Object.keys(mod);
                expect(keys).toEqual(expectedKeys);
            });
        });
    });

    describe('getModule', () => {
        test('returns an error without a module', () => {
            const result = object.getModule();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('a module name is required');
        });

        test('returns an error with a non existant module', () => {
            const result = object.getModule('bob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('module does not exist');
        });

        test('returns exsisting module and is formatted as expected', () => {
            const result = object.getModule('champion');
            const expectedKeys = ['active', 'name', 'module'];

            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');

            const keys = Object.keys(result.result);
            expect(keys).toEqual(expectedKeys);
        });

        test('gives an error if a module is not active', () => {
            const result = object.getModule('inactive');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('module is inactive');
        });
    });
});
