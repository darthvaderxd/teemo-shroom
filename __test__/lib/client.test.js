const object = require('../../lib/client');
const champion = require('../../lib/endpoints/champion');

// jest.mock('restify-clients');

describe('Client Handler > ./lib/client.js << Test', () => {
    describe('getEndPointForRegion', () => {
        test('gives an error when no region is specified', () => {
            const result = object.getEndPointForRegion();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('a region is required');
        });

        test('gives an error when region doesn\'t exist', () => {
            const result = object.getEndPointForRegion('bob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('specified region not found');
        });

        test('gives expected structure when region exists', () => {
            const result = object.getEndPointForRegion('na');
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');
        });
    });

    describe('getApiKey', () => {
        it('returns the api key set', () => {
            const result = object.getApiKey();
            expect(typeof result).toBe('string');
        });
    });

    describe('setApiKey', () => {
        it('sets the key and getApi returns expected value', () => {
            const expectedKey = 'bob';
            const oldKey = object.getApiKey();
            object.setApiKey(expectedKey);
            const result = object.getApiKey();
            expect(result).toBe(expectedKey);

            object.setApiKey(oldKey);
        });
    });

    describe('generateLinkForRegion', () => {
        test('gives an error when no region is specified', () => {
            const result = object.generateLinkForRegion();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('a region is required');
        });

        test('gives an error when region doesn\'t exist', () => {
            const result = object.generateLinkForRegion('bob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('specified region not found');
        });

        test('gives expected structure when region exists', () => {
            const result = object.generateLinkForRegion('na');
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');

            const expectedResult = {
                link: 'https://na1.api.riotgames.com',
                region: 'na',
                name: 'North America',
            };
            const expectedKeys = ['link', 'region', 'name'];
            const keys = Object.keys(result.result);
            expect(keys).toEqual(expectedKeys);
            expect(result.result).toEqual(expectedResult);
        });
    });

    describe('request', () => {
        test('requires a module and endpoint, error testing', async () => {
            let result = await object.request();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('region and endpoint required');

            result = await object.request('na');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('region and endpoint required');

            result = await object.request(null, 'champion');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('region and endpoint required');
        });

        it('gives an error message when region doesn\'t exist', async () => {
            const result = await object.request('bob', 'champion');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('specified region not found');
        });

        test('endPoint should be an object error', async () => {
            const result = await object.request('na', 'champion');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('expected endpoint to be an object');
        });

        test('returns expected result with proper data', async () => {
            const endPoint = champion();
            const result = await object.request('na', endPoint);
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');
        });
    });
});
