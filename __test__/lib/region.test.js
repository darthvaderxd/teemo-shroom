const object = require('../../lib/region');

describe('Region Module > ./lib/region.js << Test', () => {
    describe('getRegions', () => {
        it('getRegions should return an object and should follow expected structure', () => {
            const result = object.getRegions();
            expect(typeof result).toBe('object');

            const keys = Object.keys(result);
            keys.forEach((key) => {
                const childKeys = Object.keys(result[key]);
                expect(typeof result[key]).toBe('object');
                expect(typeof result[key].endpoint).toBe('string');
                expect(typeof result[key].name).toBe('string');
                expect(childKeys.length).toBe(2);
            });
        });
    });

    describe('getEndpoint', () => {
        it('should require you to specifiy a region', () => {
            const result = object.getEndpoint();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('a region is required');
        });

        it('should give you an error if region is not found', () => {
            const result = object.getEndpoint('bob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('specified region not found');
        });

        it('should give you an object if region exists', () => {
            const result = object.getEndpoint('na');
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');
        });
    });
});
