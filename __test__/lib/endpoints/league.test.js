const object = require('../../../lib/endpoints/league');

describe('Endpoint League > ./lib/endpoints/league.js << Test', () => {
    describe('getQueues', () => {
        test('returns object and matches expected format', () => {
            const result = object.getQueues();
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');

            const keys = Object.keys(result.result);
            keys.forEach((key) => {
                const expectedKeys = ['name', 'value'];
                const actualKeys = Object.keys(result.result[key]);
                expect(actualKeys).toEqual(expectedKeys);
            });
        });
    });

    describe('getRanks', () => {
        test('returns object and matches expected format', () => {
            const result = object.getRanks();
            expect(typeof result).toBe('object');
            expect(typeof result.result).toBe('object');
            expect(result.message).toBe('');

            const keys = Object.keys(result.result);
            keys.forEach((key) => {
                const expectedKeys = ['name', 'value'];
                const actualKeys = Object.keys(result.result[key]);
                expect(actualKeys).toEqual(expectedKeys);
            });
        });
    });

    describe('byQueue', () => {
        test('it throws an error if no rank is specified', () => {
            const result = object.byQueue();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specifiy a rank');
        });

        test('it throws an error if no queueType is specified', () => {
            const result = object.byQueue('bob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specifiy a queueType');
        });

        test('it throws an error if rank doesn\'t exist', () => {
            const result = object.byQueue('bob', 'rob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('the rank you listed doesn\'t exist');
        });

        test('it throws an error if rank doesn\'t exist', () => {
            const result = object.byQueue('master', 'rob');
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('the queueType you listed doesn\'t exist');
        });

        test('returns object as expected with proper rank and queueType', () => {
            const result = object.byQueue('master', 'solo');
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/league/v3/masterleagues/by-queue/RANKED_SOLO_5x5');
            expect(result.message).toBe('');
        });
    });

    describe('byLeague', () => {
        test('it throws an error if no leagueId specified', () => {
            const result = object.byLeague();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify a leagueId');
        });

        test('it throws an error if not string given for leagueId', () => {
            const result = object.byLeague(true);
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify leagueId as a string');
        });

        test('it returns object as expected if leagueId specified', () => {
            const result = object.byLeague('leagueId');
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/league/v3/leagues/leagueId');
            expect(result.message).toBe('');
        });
    });

    describe('bySummonerId', () => {
        test('it throws an error if no summonerId is specified', () => {
            const result = object.bySummonerId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specifiy a summonerId');
        });

        test('it returns an object as expected when summonerId specified', () => {
            const result = object.bySummonerId(12345);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/league/v3/positions/by-summoner/12345');
            expect(result.message).toBe('');
        });
    });
});
