const object = require('../../../lib/endpoints/mastery');

describe('Endpoint Mastery > ./lib/endpoints/mastery.js << Test', () => {
    describe('bySummonerId', () => {
        test('it throws an error if summonId is empty', () => {
            const result = object.bySummonerId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an summonerId');
        });

        test('it returns object as expected with summonerId and empty championId', () => {
            const result = object.bySummonerId(1337);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/champion-mastery/v3/champion-masteries/by-summoner/1337');
            expect(result.message).toBe('');
        });

        test('it returns object as expected with summonerId and championId', () => {
            const result = object.bySummonerId(1337, 'beef');
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/champion-mastery/v3/champion-masteries/by-summoner/1337/by-champion/beef');
            expect(result.message).toBe('');
        });
    });

    describe('getTotalScoreBySummonerId', () => {
        test('it throws an error if summonId is empty', () => {
            const result = object.getTotalScoreBySummonerId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an summonerId');
        });

        test('it returns object as expected with summonerId', () => {
            const result = object.getTotalScoreBySummonerId(1337);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/champion-mastery/v3/scores/by-summoner/1337');
            expect(result.message).toBe('');
        });
    });
});
