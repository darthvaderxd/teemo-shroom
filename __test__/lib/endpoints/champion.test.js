const object = require('../../../lib/endpoints/champion');

describe('Endpoint Champion > ./lib/endpoints/champion.js << Test', () => {
    it('returns an object with no params and matches expected format', () => {
        const result = object();
        expect(typeof result).toBe('object');
        expect(result.result).toBe('/lol/platform/v3/champions');
        expect(result.message).toBe('');
    });

    it('returns an object with params and matches expected format', () => {
        const id = 'bob';
        const result = object(id);
        expect(typeof result).toBe('object');
        expect(result.result).toBe(`/lol/platform/v3/champions/${id}`);
        expect(result.message).toBe('');
    });
});
