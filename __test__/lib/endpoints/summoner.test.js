const object = require('../../../lib/endpoints/summoner');

describe('Endpoint Spectator > ./lib/endpoints/spectator.js << Test', () => {
    describe('byAccountId', () => {
        test('it throws an error if accountId is empty', () => {
            const result = object.byAccountId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an accountId');
        });

        test('it returns object as expected with accountId', () => {
            const result = object.byAccountId(1337);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/summoner/v3/summoners/by-account/1337');
            expect(result.message).toBe('');
        });
    });

    describe('bySummonerName', () => {
        test('it throws an error if summonerName is empty', () => {
            const result = object.bySummonerName();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an summonerName');
        });

        test('it returns object as expected with summonerName', () => {
            const result = object.bySummonerName('darthxvaderxd');
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/summoner/v3/summoners/by-name/darthxvaderxd');
            expect(result.message).toBe('');
        });
    });

    describe('bySummonerId', () => {
        test('it throws an error if summonerId is empty', () => {
            const result = object.bySummonerId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an summonerId');
        });

        test('it returns object as expected with summonerId', () => {
            const result = object.bySummonerId(221292);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/summoner/v3/summoners/221292');
            expect(result.message).toBe('');
        });
    });
});
