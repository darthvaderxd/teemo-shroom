const object = require('../../../lib/endpoints/match');

describe('Endpoint Status > ./lib/endpoints/status.js << Test', () => {
    describe('byAccountId', () => {
        test('throws an error if accountId empty', () => {
            const result = object.byAccountId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an accountId');
        });

        test('gives object as expected with accountId specified', () => {
            const result = object.byAccountId(34);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/match/v3/matchlists/by-account/34');
            expect(result.message).toBe('');
        });
    });

    describe('byMatchId', () => {
        test('throws an error if matchId empty', () => {
            const result = object.byMatchId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify a matchId');
        });

        test('returns object as expected with matchId and no withTimeline', () => {
            const result = object.byMatchId(123456);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/match/v3/matches/123456');
            expect(result.message).toBe('');
        });

        test('returns object as expected with matchId and no withTimelines', () => {
            const result = object.byMatchId(123456, true);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/match/v3/timelines/by-match/123456');
            expect(result.message).toBe('');
        });
    });

    describe('byTournamentId', () => {
        it('throws an error if tournamentId is empty', () => {
            const result = object.byTournamentId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specifiy a tournamentId');
        });

        it('returns object as epxected with tornamentId and no matchId', () => {
            const result = object.byTournamentId(123);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/match/v3/matches/by-tournament-code/123/ids');
            expect(result.message).toBe('');
        });

        it('returns object as epxected with tornamentId and no matchId', () => {
            const result = object.byTournamentId(123, 456);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/match/v3/matches/456/by-tournament-code/123');
            expect(result.message).toBe('');
        });
    });
});
