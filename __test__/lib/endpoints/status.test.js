const object = require('../../../lib/endpoints/status');

describe('Endpoint Status > ./lib/endpoints/status.js << Test', () => {
    it('returns object as expected', () => {
        const result = object();
        expect(typeof result).toBe('object');
        expect(result.result).toBe('/lol/status/v3/shard-data');
        expect(result.message).toBe('');
    });
});
