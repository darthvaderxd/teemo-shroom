const object = require('../../../lib/endpoints/spectator');

describe('Endpoint Spectator > ./lib/endpoints/spectator.js << Test', () => {
    describe('bySummonerId', () => {
        test('it throws an error if summonerId is empty', () => {
            const result = object.bySummonerId();
            expect(typeof result).toBe('object');
            expect(result.result).toBe(false);
            expect(result.message).toBe('you must specify an summonerId');
        });

        test('it returns object as expected with summonerId', () => {
            const result = object.bySummonerId(1337);
            expect(typeof result).toBe('object');
            expect(result.result).toBe('/lol/spectator/v3/active-games/by-summoner/1337');
            expect(result.message).toBe('');
        });
    });

    describe('featuredGames', () => {
        test('it returns object as expected', () => {
            const result = object.featuredGames();
            expect(result.result).toBe('/lol/spectator/v3/featured-games');
            expect(result.message).toBe('');
        });
    });
});
